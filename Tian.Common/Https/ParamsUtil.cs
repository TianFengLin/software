﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tian.Common.Https
{
    /// <summary>
    /// 参数方法
    /// </summary>
    public class ParamsUtil
    {
        /// <summary>
        /// 获取文件上传的boundary
        /// </summary>
        /// <returns>文件上传的boundary</returns>
        public string GetUrlBoundary()
        {
            string random = DateTime.Now.Ticks.ToString("x");
            string boundary = "------WebKitFormBoundary" + random;
            return boundary;
        }
    }
}
