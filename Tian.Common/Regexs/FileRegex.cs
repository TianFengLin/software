﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Tian.Common.Enums;
using Tian.Common.Enums.RegexEnum;

namespace Tian.Common.Regexs
{
    /// <summary>
    /// 文件相关正则
    /// </summary>
    public class FileRegex
    {
        /// <summary>
        /// 从文件路径中匹配内容不能包含符号：()
        /// </summary>
        /// <param name="filepath">文件路径</param>
        /// <param name="filetype">匹配规则</param>
        /// <returns>匹配结果</returns>
        public string RegexFilPath(string filepath, FilePath filetype)
        {
            string result = string.Empty;
            Regex regex = new Regex(@"^(?<fpath>([a-zA-Z]:\\)([\s\.\-\w]+\\)*)(?<fname>[\w]+)(?<namext>(\.[\w]+)*)(?<suffix>\.[\w]+)");
            Match match = regex.Match(filepath);
            switch (filetype)
            {
                case FilePath.NameOnly:
                    result = match.Result("${fname}") + match.Result("${namext}");
                    break;
                case FilePath.FileName:
                    result = match.Result("${fname}") + match.Result("${namext}") + match.Result("${suffix}");
                    break;
                case FilePath.FileSuffix:
                    result = match.Result("${suffix}");
                    break;
                case FilePath.PathExceptName:
                    result = match.Result("${fpath}");
                    break;
                case FilePath.PathExceptSuffix:
                    result = match.Result("${fpath}") + match.Result("${fname}") + match.Result("${namext}");
                    break;
                default:
                    result = filepath;
                    break;
            }
            return result;
        }
    }
}
