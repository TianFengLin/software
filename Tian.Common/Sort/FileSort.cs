﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Tian.Common.Sort
{
    /// <summary>
    /// 文件排序
    /// </summary>
    public class FileSort
    {
        [System.Runtime.InteropServices.DllImport("Shlwapi.dll", CharSet = System.Runtime.InteropServices.CharSet.Unicode)]
        private static extern int StrCmpLogicalW(string fileone, string filetwo);
        /// <summary>
        /// 根据文件名顺序排序(windows排序)
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortByNameAsc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return StrCmpLogicalW(one.Name, two.Name); });
            return files;
        }
        /// <summary>
        /// 根据文件名倒序排序
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortByNameDesc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return StrCmpLogicalW(two.Name, one.Name); });
            return files;
        }

        /// <summary>
        /// 根据文件大小顺序排序
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortBySizeAsc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return two.Length.CompareTo(one.Length); });
            return files;
        }
        /// <summary>
        /// 根据文件大小倒序排序
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortBySizeDesc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return one.Length.CompareTo(two.Length); });
            return files;
        }

        /// <summary>
        /// 根据文件创建时间顺序排序
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortByCreatTimeAsc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return two.CreationTime.CompareTo(one.CreationTime); });
            return files;
        }
        /// <summary>
        /// 根据文件创建时间倒序排序
        /// </summary>
        /// <param name="files"></param>
        public List<FileInfo> FileSortByCreatTimeDesc(ref List<FileInfo> files)
        {
            files.Sort((one, two) => { return one.CreationTime.CompareTo(two.CreationTime); });
            return files;
        }
    }
}
