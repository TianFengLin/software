﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tian.Common.Sort
{
    /// <summary>
    /// 字符串排序
    /// </summary>
    public class StringSort
    {
        /// <summary>
        /// 根据长度正排
        /// </summary>
        public class StringSortByLengthAsc : IComparer<string>
        {
            /// <summary>
            /// 根据字符长度排序
            /// </summary>
            /// <param name="a">字符1</param>
            /// <param name="b">字符2</param>
            /// <returns></returns>
            public int Compare(string a, string b)
            {
                string[] str1 = a.Split('\\');
                string[] str2 = b.Split('\\');
                if (int.Parse(str1[str1.Length - 1]) > int.Parse(str2[str2.Length - 1]))
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }
        /// <summary>
        /// 根据长度倒排
        /// </summary>
        public class StringSortByLengthDesc : IComparer<string>
        {
            /// <summary>
            /// 根据字符长度排序
            /// </summary>
            /// <param name="a">字符1</param>
            /// <param name="b">字符2</param>
            /// <returns></returns>
            public int Compare(string a, string b)
            {
                string[] str1 = a.Split('\\');
                string[] str2 = b.Split('\\');
                if (int.Parse(str1[str1.Length - 1]) > int.Parse(str2[str2.Length - 1]))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }
    }
}
