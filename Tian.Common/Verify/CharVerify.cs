﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Tian.Common.Verify
{
    /// <summary>
    /// 字符验证
    /// </summary>
    public static class CharVerify
    {
        /// <summary>
        /// 是否为空
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty(string value)
        {
            if (value == null || value.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 是否为空
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty(int value)
        {

            if (value == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool IsNumber(string value)
        {
            Regex regex = new Regex("^[0-9]*[1-9][0-9]*$");
            return regex.IsMatch(value);
        }
    }
}
