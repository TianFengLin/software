﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Tian.Common.Verify
{
    /// <summary>
    /// 文件类验证
    /// </summary>
    public class FileVerify
    {
        /// <summary>
        /// 文件是否相同
        /// </summary>
        /// <param name="pathOne">文件路径</param>
        /// <param name="pathTwo">文件路径</param>
        /// <returns></returns>
        public bool IsSame(string pathOne, string pathTwo)
        {
            //计算第一个文件的哈希值
            var hash = System.Security.Cryptography.HashAlgorithm.Create();
            var stream_1 = new FileStream(pathOne, FileMode.Open);
            byte[] hashByte_1 = hash.ComputeHash(stream_1);
            stream_1.Close();
            //计算第二个文件的哈希值
            var stream_2 = new FileStream(pathTwo, FileMode.Open);
            byte[] hashByte_2 = hash.ComputeHash(stream_2);
            stream_2.Close();
            //比较两个哈希值
            if (BitConverter.ToString(hashByte_1) == BitConverter.ToString(hashByte_2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 文件是否相同
        /// </summary>
        /// <param name="fileOne">文件</param>
        /// <param name="fileTwo">文件</param>
        /// <returns></returns>
        public bool IsSame(FileInfo fileOne, FileInfo fileTwo)
        {
            //计算第一个文件的哈希值
            var hash = System.Security.Cryptography.HashAlgorithm.Create();
            FileStream stream_1 = fileOne.OpenRead();
            byte[] hashByte_1 = hash.ComputeHash(stream_1);
            stream_1.Close();
            //计算第二个文件的哈希值
            FileStream stream_2 = fileTwo.OpenRead();
            byte[] hashByte_2 = hash.ComputeHash(stream_2);
            stream_2.Close();
            //比较两个哈希值
            if (BitConverter.ToString(hashByte_1) == BitConverter.ToString(hashByte_2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
