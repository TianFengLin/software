﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tian.Common.Texts
{
    public class StringUtil
    {
        /// <summary>
        /// 移除最后一个字符
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveLastChar(string value)
        {
            return value.Remove(value.Length - 1);
        }
        /// <summary>
        /// 获取最后一个字符
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetLastChar(string value)
        {
            return value.Substring(value.Length - 1, 1);
        }
    }
}
