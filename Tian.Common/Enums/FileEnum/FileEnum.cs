﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tian.Common.Enums.FileEnum
{
    /// <summary>
    /// 文件类型
    /// </summary>
    public enum FileEnum
    {
        /// <summary>
        /// 图片
        /// </summary>
        Image = 1,
        /// <summary>
        /// 视频
        /// </summary>
        Video = 2,
    }
}
