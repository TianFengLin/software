﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tian.Common.Enums.HttpEnum
{
    /// <summary>
    /// http访问类型
    /// </summary>
    public enum RequestMothed
    {
        /// <summary>
        /// Get请求
        /// </summary>
        Get = 1,
        /// <summary>
        /// Post请求
        /// </summary>
        Post = 2,
        /// <summary>
        /// Put请求
        /// </summary>
        Put = 3,
        /// <summary>
        /// Delete请求
        /// </summary>
        Delete = 4
    }
}
