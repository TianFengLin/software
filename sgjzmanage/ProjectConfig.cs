﻿using Newtonsoft.Json.Linq;
using sgjzmanage.config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sgjzmanage
{
    public static class ProjectConfig
    {
        /// <summary>
        /// 获取数据库配置
        /// </summary>
        /// <returns></returns>
        public static void GetDataBaseSetting()
        {
            string path = System.Windows.Forms.Application.StartupPath + Path.DirectorySeparatorChar + "application.json";
            string json = File.ReadAllText(path, Encoding.UTF8);
            JObject jo = JObject.Parse(json);
            string modPath = jo["modPath"].ToString();
            string moPath = jo["moPath"].ToString();
            AppSetting.modPath = modPath;
            AppSetting.moPath = moPath;
        }
    }
}
