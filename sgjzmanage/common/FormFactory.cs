﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace sgjzmanage.common
{
    public static class FormFactory
    {
        public static List<Type> formList;
        /// <summary>
        /// 获取窗体集合
        /// </summary>
        public static void GetFromList()
        {
            Assembly assembly = Assembly.Load("sgjzmanage");
            formList = assembly.GetTypes().Where(t => typeof(Page).IsAssignableFrom(t)).ToList();
            //formList = assembly.GetTypes().Where(t => typeof(UserControl).IsAssignableFrom(t)).ToList();
        }
        /// <summary>
        /// 根据插件类名称获取插件
        /// </summary>
        /// <param name="formName">插件类名称</param>
        /// <returns>插件对象</returns>
        public static object GetFormByName(string formName)
        {
            Type type = formList.Find(t => t.Name == formName);
            object form = Activator.CreateInstance(type);
            return form;
        }
    }
}
