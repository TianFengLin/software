﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace sgjzmanage.model
{
    public class SysMenu
    {
        private string menuId;
        private string menuName;
        private string isRoot;
        private string isDocument;
        private string path;
        private Brush color;
        private List<SysMenu> children;
        /// <summary>
        /// 菜单编号
        /// </summary>
        public string MenuId { get => menuId; set => menuId = value; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get => menuName; set => menuName = value; }
        /// <summary>
        /// 是否是子节点
        /// </summary>
        public string IsRoot { get => isRoot; set => isRoot = value; }
        /// <summary>
        /// 子菜单
        /// </summary>
        public List<SysMenu> Children { get => children; set => children = value; }
        /// <summary>
        /// 是否文档目录
        /// </summary>
        public string IsDocument { get => isDocument; set => isDocument = value; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get => path; set => path = value; }
        /// <summary>
        /// 文本颜色
        /// </summary>
        public Brush Color { get => color; set => color = value; }
    }
}
