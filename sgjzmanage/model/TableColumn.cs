﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace sgjzmanage.model
{
    /// <summary>
    /// 表字段属性
    /// </summary>
    public class TableColumn
    {
        private string columnName;
        private Type columnType;
        private string propertyName;
        private Type propertyType;
        private PropertyInfo propertyValue;
        /// <summary>
        /// 列名称
        /// </summary>
        public string ColumnName { get => columnName; set => columnName = value; }
        /// <summary>
        /// 列类型
        /// </summary>
        public Type ColumnType { get => columnType; set => columnType = value; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string PropertyName { get => propertyName; set => propertyName = value; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public Type PropertyType { get => propertyType; set => propertyType = value; }
        /// <summary>
        /// 字段属性
        /// </summary>
        public PropertyInfo PropertyValue { get => propertyValue; set => propertyValue = value; }
    }
}
