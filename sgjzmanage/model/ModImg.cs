﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sgjzmanage.model
{
    public class ModImg
    {
        private string path;
        private string isGif;

        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get => path; set => path = value; }
        /// <summary>
        /// 是否gif
        /// </summary>
        public string IsGif { get => isGif; set => isGif = value; }
    }
}
