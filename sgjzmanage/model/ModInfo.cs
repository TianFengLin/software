﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sgjzmanage.model
{
    public class ModInfo
    {
        private string url;

        private string catalogue;

        private string version;

        private string description;

        private string modName;

        private List<ModImg> images;


        /// <summary>
        /// 网址
        /// </summary>
        public string Url { get => url; set => url = value; }

        /// <summary>
        /// 目录
        /// </summary>
        public string Catalogue { get => catalogue; set => catalogue = value; }

        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get => version; set => version = value; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get => description; set => description = value; }

        /// <summary>
        /// 图片
        /// </summary>
        public List<ModImg> Images { get => images; set => images = value; }

        /// <summary>
        /// 包名称
        /// </summary>
        public string ModName { get => modName; set => modName = value; }
    }
}
