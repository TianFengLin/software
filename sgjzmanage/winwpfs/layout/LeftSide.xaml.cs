﻿using Newtonsoft.Json.Linq;
using sgjzmanage.config;
using sgjzmanage.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Tian.Common.Base;

namespace sgjzmanage.winwpfs.layout
{
    /// <summary>
    /// LeftSide.xaml 的交互逻辑
    /// </summary>
    public partial class LeftSide : UserControl
    {
        public Action<SysMenu> ClickMenu { get; set; }
        public List<string> menuRoots = new List<string>();
        public List<string> moNames = new List<string>();
        public LeftSide()
        {
            InitializeComponent();
            GetMenus();
        }
        public LeftSide(Action<SysMenu> clickMenu)
        {
            InitializeComponent();
            GetMenus();
            ClickMenu = clickMenu;
        }
        public List<SysMenu> Menus { get; set; }
        private void GetMenus()
        {
            //获取数据库配置
            ProjectConfig.GetDataBaseSetting();
            // 获取mo已安装的mod列表
            GetMoModsList();
            List<SysMenu> menuData = GetDataBaseSetting("");
            menuBuild(ref menuData);
            this.menuTree.ItemsSource = menuData;
        }

        private void GetMenusReload(object sender, RoutedEventArgs e)
        {
            List<SysMenu> menuData = GetDataBaseSetting("");
            menuBuild(ref menuData);
            this.menuTree.ItemsSource = menuData;
            MessageBoxResult messageBoxResult = HandyControl.Controls.MessageBox.Show("操作已完成", "操作提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void GetMoReload(object sender, RoutedEventArgs e)
        {
            List<string> modNames = new List<string>();
            string modsPath = AppSetting.moPath + Path.DirectorySeparatorChar + "mods";

            if (Directory.Exists(modsPath))
            {
                DirectoryInfo dir = new DirectoryInfo(modsPath);
                DirectoryInfo[] dirsList = dir.GetDirectories();
                foreach (DirectoryInfo dirInfo in dirsList)
                {
                    modNames.Add(dirInfo.Name);
                }
                moNames = modNames;
                string savePath = AppSetting.modPath + Path.DirectorySeparatorChar + "modList.txt";
                File.WriteAllLines(savePath, modNames, Encoding.UTF8);
            }
            MessageBoxResult messageBoxResult = HandyControl.Controls.MessageBox.Show("操作已完成", "操作提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void GetModReload(object sender, RoutedEventArgs e)
        {
            foreach (string menuRoot in menuRoots)
            {
                List<string> modNames = new List<string>();
                DirectoryInfo dir = new DirectoryInfo(menuRoot);
                DirectoryInfo[] dirsList = dir.GetDirectories();
                foreach (DirectoryInfo dirInfo in dirsList)
                {
                    string modPath = dirInfo.FullName + Path.DirectorySeparatorChar + "modInfo.json";
                    if (File.Exists(modPath))
                    {
                        string json = File.ReadAllText(modPath, Encoding.UTF8);
                        JObject jo = JObject.Parse(json);
                        if (jo["modName"] != null)
                        {
                            string modName = jo["modName"].ToString();
                            if (modName.Trim() != "")
                            {
                                modNames.Add(dirInfo.Name + "@" + modName);
                            }
                        }

                    }
                }
                string savePath = menuRoot + Path.DirectorySeparatorChar + "modList.txt";
                File.WriteAllLines(savePath, modNames, Encoding.UTF8);
            }
            MessageBoxResult messageBoxResult = HandyControl.Controls.MessageBox.Show("操作已完成", "操作提示", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void menuTree_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            SysMenu menu = (SysMenu)this.menuTree.SelectedItem;
            if (menu != null)
            {
                if (menu.IsRoot == "0")
                {
                    ClickMenu.Invoke(menu);
                }
            }

        }

        public void menuBuild(ref List<SysMenu> menuData)
        {

            foreach (SysMenu menu in menuData)
            {

                if (menu.IsRoot == "1")
                {
                    string modpath = AppSetting.modPath + Path.DirectorySeparatorChar + menu.Path + Path.DirectorySeparatorChar + "config.json";
                    if (File.Exists(modpath))
                    {
                        List<SysMenu> menuMods = GetDataBaseSetting(modpath);
                        menuBuild(ref menuMods);
                        menu.Children = menuMods;
                    }

                }
                else
                {
                    if (menu.IsDocument == "0")
                    {
                        List<string> modNames = new List<string>();
                        string modsPath = AppSetting.modPath + Path.DirectorySeparatorChar + menu.Path + Path.DirectorySeparatorChar + "modList.txt";
                        if (File.Exists(modsPath))
                        {
                            string[] names = File.ReadAllLines(modsPath, Encoding.UTF8);
                            foreach (string name in names)
                            {
                                if (name != "")
                                {
                                    string[] data = name.Split("@");
                                    if (moNames.Contains(data[1]))
                                    {
                                        modNames.Add(data[0]);
                                    }

                                }
                            }

                        }

                        List<SysMenu> menuMods = new List<SysMenu>();
                        //读取下面目录为节点
                        string modpath = AppSetting.modPath + Path.DirectorySeparatorChar + menu.Path;
                        menuRoots.Add(modpath);//添加路径
                        DirectoryInfo dir = new DirectoryInfo(modpath);
                        DirectoryInfo[] dirsList = dir.GetDirectories();
                        foreach (DirectoryInfo dirInfo in dirsList)
                        {
                            SysMenu sysMenu = new SysMenu();
                            sysMenu.MenuId = dirInfo.Name;
                            sysMenu.MenuName = dirInfo.Name;
                            sysMenu.IsRoot = "0";
                            sysMenu.IsDocument = "0";
                            sysMenu.Path = dirInfo.FullName;
                            sysMenu.Color = Brushes.Black;
                            if (modNames.Contains(dirInfo.Name))
                            {
                                sysMenu.Color = Brushes.Red;
                            }
                            menuMods.Add(sysMenu);
                        }
                        menu.Children = menuMods;
                    }

                }
            }
        }

        /// <summary>
        /// 获取数据库配置
        /// </summary>
        /// <returns></returns>
        public List<SysMenu> GetDataBaseSetting(string modPath)
        {
            List<SysMenu> menuData = new List<SysMenu>();
            if (modPath == "")
            {
                modPath = AppSetting.modPath + Path.DirectorySeparatorChar + "config.json";
            }
            string json = File.ReadAllText(modPath, Encoding.UTF8);
            JObject jo = JObject.Parse(json);
            string dataBases = jo["menu"].ToString();
            JArray array = JArray.Parse(dataBases);
            for (int i = 0; i < array.Count; i++)
            {
                SysMenu menu = new SysMenu();
                menu.MenuId = array[i]["menuId"].ToString();
                menu.MenuName = array[i]["menuName"].ToString();
                if (array[i]["isDocument"] == null)
                {
                    menu.IsDocument = "0";
                }
                else
                {
                    menu.IsDocument = array[i]["isDocument"].ToString();
                }
                menu.Color = Brushes.Black;
                menu.Path = array[i]["path"].ToString();
                menu.IsRoot = array[i]["isRoot"].ToString();
                menuData.Add(menu);
            }
            return menuData;
        }
        /// <summary>
        /// 获取mo安装的mod文件夹名称
        /// </summary>
        public void GetMoModsList()
        {
            string modsPath = AppSetting.modPath + Path.DirectorySeparatorChar + "modList.txt";
            if (File.Exists(modsPath))
            {
                moNames.Clear();
                string[] names = File.ReadAllLines(modsPath, Encoding.UTF8);
                moNames.AddRange(names);
            }
        }
    }
}
