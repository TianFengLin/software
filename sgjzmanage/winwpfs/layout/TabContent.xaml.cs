﻿using MaterialDesignThemes.Wpf;
using Newtonsoft.Json.Linq;
using PdfiumViewer;
using sgjzmanage.common;
using sgjzmanage.config;
using sgjzmanage.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using Tian.Common.Base;
using WpfAnimatedGif;

namespace sgjzmanage.winwpfs.layout
{
    /// <summary>
    /// TabContent.xaml 的交互逻辑
    /// </summary>
    public partial class TabContent : UserControl
    {
        public TabContent()
        {
            InitializeComponent();
            InitializeAsync();
            FormFactory.GetFromList();
        }
        private Thread ImageThread;
        private ModInfo CurrentMod;
        /// <summary>
        /// 添加tab页
        /// </summary>
        /// <param name="menu">菜单</param>
        public void AddTab(SysMenu menu)
        {
            //mod信息
            if (menu.IsDocument == "0")
            {
                ModInfoArea.Visibility = Visibility.Visible;
                ModDocumentArea.Visibility = Visibility.Collapsed;
                string configPath = menu.Path + Path.DirectorySeparatorChar + "modInfo.json";
                if (!File.Exists(configPath))
                {
                    return;
                }
                //获取信息
                string json = File.ReadAllText(configPath, Encoding.UTF8);
                JObject jo = JObject.Parse(json);
                ModInfo modInfo = new ModInfo();
                if (jo["url"] != null)
                {
                    modInfo.Url = jo["url"].ToString();
                }
                if (jo["version"] != null)
                {
                    modInfo.Version = jo["version"].ToString();
                }
                if (jo["modName"] != null)
                {
                    modInfo.ModName = jo["modName"].ToString();
                }
                modInfo.Catalogue = menu.Path;

                // 获取描述信息 先看是否有readme.txt
                string readmePath = menu.Path + Path.DirectorySeparatorChar + "readme.txt";
                if (File.Exists(readmePath))
                {
                    TxtPreview.IsEnabled = true;
                    string description = "";
                    string[] descriptions = File.ReadAllLines(readmePath);
                    if (descriptions.Length > 0)
                    {
                        description = string.Join("\n", descriptions);
                        if (description.Length > 320)
                        {
                            description = description.Substring(0, 320) + "......";
                        }
                    }
                    modInfo.Description = description;
                }
                else
                {
                    TxtPreview.IsEnabled = false;
                }
                // 获取帮助文档 先看是否有document.pdf
                string documentPath = menu.Path + Path.DirectorySeparatorChar + "document.pdf";
                if (File.Exists(documentPath))
                {
                    HtmlPreview.IsEnabled = true;
                }
                else
                {

                    HtmlPreview.IsEnabled = false;
                }
                CurrentMod = modInfo;
                //渲染信息
                Url.Text = modInfo.Url;
                Catalogue.Text = modInfo.Catalogue;
                Version.Text = modInfo.Version;
                Description.Text = modInfo.Description;
                ModName.Text = modInfo.ModName;
                ImagePanel.Children.Clear();
                //获取照片
                ImageThread = new Thread(new ThreadStart(() => { InitImages(modInfo); }));
                ImageThread.Start();
            }
            else if (menu.IsDocument == "1")
            {
                ModInfoArea.Visibility = Visibility.Collapsed;
                ModDocumentArea.Visibility = Visibility.Visible;
                //文档
                string documentPath = AppSetting.modPath + Path.DirectorySeparatorChar + menu.Path + Path.DirectorySeparatorChar + "document.pdf";
                if (!File.Exists(documentPath))
                {
                    return;
                }
                WebBrowser.Source = new Uri(documentPath);
            }

        }
        /// <summary>
        /// 删除tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void removeTab(object sender, MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// 预览图片
        /// </summary>
        /// <param name="modInfo"></param>
        /// <param name="imageList"></param>
        public void InitImages(ModInfo modInfo)
        {
            Thread.Sleep(200);//防止过长
            List<ModImg> imageList = new List<ModImg>();
            FileUtil fileUtil = new FileUtil();
            List<string> suffixs = new List<string>() { "jpg", "png", "gif" };
            List<FileInfo> fileInfos = fileUtil.GetAllFilesInPath(modInfo.Catalogue, suffixs, true);

            if (fileInfos.Count > 0)
            {
                foreach (FileInfo fileInfo in fileInfos)
                {
                    ModImg modImg = new ModImg();
                    modImg.Path = fileInfo.FullName;
                    string[] list = fileInfo.Name.Split('.');
                    string houzhui = list[list.Length - 1].ToLower();
                    if (houzhui == "gif")
                    {
                        modImg.IsGif = "1";
                    }
                    else
                    {
                        modImg.IsGif = "0";
                    }
                    imageList.Add(modImg);
                }
            }
            foreach (ModImg modImg in imageList)
            {
                ImagePanel.Dispatcher.InvokeAsync(() =>
                {
                    BitmapImage bitmapImage = new BitmapImage();
                    Image img = new Image();
                    img.Margin = new Thickness(0, 10, 0, 0);
                    img.Stretch = Stretch.UniformToFill;

                    bitmapImage.BeginInit();
                    bitmapImage.UriSource = new Uri(modImg.Path, UriKind.RelativeOrAbsolute);
                    bitmapImage.EndInit();
                    ImageSource Source = bitmapImage;
                    if (modImg.IsGif == "1")
                    {
                        ImageBehavior.SetAnimatedSource(img, Source);
                    }
                    else
                    {
                        img.Source = bitmapImage;
                    }
                    ImagePanel.Children.Add(img);
                });

            }

        }
        public void closeRedColor(object sender, MouseEventArgs e)
        {
            PackIcon packIcon = (PackIcon)sender;
            packIcon.Background = Brushes.Red;
        }
        public void closeBlackColor(object sender, MouseEventArgs e)
        {
            PackIcon packIcon = (PackIcon)sender;
            packIcon.Background = Brushes.Transparent;

        }

        public void PreviewHtmlDocument(object sender, RoutedEventArgs e)
        {
            HtmlWindow htmlWindow = new HtmlWindow();
            string url = CurrentMod.Catalogue + Path.DirectorySeparatorChar + "document.pdf";
            htmlWindow.SetUrl(url);
            htmlWindow.Show();
        }

        public void PreviewTxtDocument(object sender, RoutedEventArgs e)
        {
            HtmlWindow htmlWindow = new HtmlWindow();
            string url = CurrentMod.Catalogue + Path.DirectorySeparatorChar + "readme.txt";
            htmlWindow.SetUrl(url);
            htmlWindow.Show();
        }

        /// <summary>
        /// 初始化浏览器
        /// </summary>
        async public void InitializeAsync()
        {
            await WebBrowser.EnsureCoreWebView2Async(null);
        }
    }
}