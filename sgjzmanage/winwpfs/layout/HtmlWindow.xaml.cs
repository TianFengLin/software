﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace sgjzmanage.winwpfs.layout
{
    /// <summary>
    /// HtmlWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HtmlWindow : Window
    {
        public HtmlWindow()
        {
            InitializeComponent();
            InitializeAsync();
        }

        public void SetUrl(string url)
        {
            WebBrowser.Source = new Uri(url);
            //WebBrowser.Navigate(url);
        }
        async public void InitializeAsync()
        {
            await WebBrowser.EnsureCoreWebView2Async(null);
        }
    }
}
