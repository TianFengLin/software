﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tian.Common.Verify;

namespace software.common
{
    public class CreateSqlUtil
    {
        /// <summary>
        /// 空值转换为null的字段
        /// </summary>
        public List<string> emptyIsNulls;
        /// <summary>
        /// 需要转义的字段
        /// </summary>
        public List<string> escapeFields;
        /// <summary>
        /// 把table column的value值转化成执行语句的value值
        /// </summary>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        public string GetValueByColumnValue(string columnKey, object columnValue)
        {
            string realValue = columnValue.ToString();
            if (emptyIsNulls.Contains(columnKey))
            {
                if (CharVerify.IsEmpty(realValue))
                {
                    return "null";
                }
            }
            if (escapeFields.Contains(columnKey))
            {
                realValue = realValue.Replace("'", @"\'");
            }
            return "'" + realValue + "'";

        }
    }
}
