﻿using software.config;
using software.config.model;
using software.connect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace software.common
{
    public static class SqlFactory
    {
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <param name="config">配置信息</param>
        /// <param name="dataBaseType">数据库类型</param>
        /// <returns></returns>
        public static ISqlConnect GetSqlConnect(string key)
        {
            ISqlConnect sqlConnect = null;
            DataBase dataBase = AppSetting.dataBase.Find(n => n.Key == key);
            string connectValue = "";
            if (dataBase.BaseType == "mssql")
            {
                connectValue = string.Format("Data Source=.;Initial Catalog={0};uid={1};pwd={2};", dataBase.BaseName, dataBase.UserName, dataBase.Password);
                sqlConnect = new SqlConnect_Ms(connectValue);
            }
            else if (dataBase.BaseType == "mysql")
            {
                connectValue = string.Format("server={0};port={1};user={2};password={3}; database={4};character set=utf8;", dataBase.Ip, dataBase.Port, dataBase.UserName, dataBase.Password, dataBase.BaseName);
                sqlConnect = new SqlConnect_My(connectValue);
            }
            return sqlConnect;
        }
    }
}
