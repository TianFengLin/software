﻿using MaterialDesignThemes.Wpf;
using software.common;
using software.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml;

namespace software.winwpfs.layout
{
    /// <summary>
    /// TabContent.xaml 的交互逻辑
    /// </summary>
    public partial class TabContent : UserControl
    {
        public TabContent()
        {
            InitializeComponent();
            FormFactory.GetFromList();
        }
        /// <summary>
        /// 添加tab页
        /// </summary>
        /// <param name="menu">菜单</param>
        public void AddTab(SysMenu menu)
        {
            //判断是否已存在，存在则选中
            for (int i = 0; i < this.tabControls.Items.Count; i++)
            {
                TabItem tabContent = (TabItem)this.tabControls.Items[i];
                if (tabContent.Name == ("menu" + menu.MenuId))
                {
                    this.tabControls.SelectedIndex = i;
                    return;
                }
            }
            //加载模板
            TabItem itemTemplate = this.FindResource("TabItemTemplate") as TabItem;
            string xaml = System.Windows.Markup.XamlWriter.Save(itemTemplate);
            TabItem tabItem = System.Windows.Markup.XamlReader.Parse(xaml) as TabItem;      
            StackPanel stackPanel = tabItem.Header as StackPanel;
            TextBlock textBlock = ElementUtil.GetChildrenByType<TextBlock>(stackPanel);
            textBlock.Text = menu.MenuName;
            List<PackIcon> list = ElementUtil.GetChildrensByType<PackIcon>(stackPanel);
            list[0].Kind = PackIconKind.Folder;
            list[1].Tag = "menu" + menu.MenuId;
            list[1].MouseDown += removeTab;
            list[1].MouseEnter += closeRedColor;
            list[1].MouseLeave += closeBlackColor;
            tabItem.Name = "menu" + menu.MenuId;
            Frame frame = tabItem.Content as Frame;
            frame.Content = FormFactory.GetFormByName(menu.Url);

            this.tabControls.Items.Add(tabItem);
            this.tabControls.RegisterName("menu" + menu.MenuId, tabItem);
            this.tabControls.SelectedIndex = this.tabControls.Items.Count - 1;
        }
        /// <summary>
        /// 删除tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void removeTab(object sender, MouseButtonEventArgs e)
        {
            PackIcon packIcon = (PackIcon)sender;
            string tabName = packIcon.Tag.ToString();
            TabItem tabItem = (TabItem)this.tabControls.FindName(tabName);
            this.UnregisterName(tabName);
            this.tabControls.Items.Remove(tabItem);
        }
        public void closeRedColor(object sender, MouseEventArgs e)
        {
            PackIcon packIcon = (PackIcon)sender;
            packIcon.Background = Brushes.Red;
        }
        public void closeBlackColor(object sender, MouseEventArgs e)
        {
            PackIcon packIcon = (PackIcon)sender;
            packIcon.Background = Brushes.Transparent;

        }
    }
}