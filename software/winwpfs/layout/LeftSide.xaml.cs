﻿using software.common;
using software.connect;
using software.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace software.winwpfs.layout
{
    /// <summary>
    /// LeftSide.xaml 的交互逻辑
    /// </summary>
    public partial class LeftSide : UserControl
    {
        public Action<SysMenu> ClickMenu { get; set; }
        public LeftSide()
        {
            InitializeComponent();
            GetMenus();
        }
        public LeftSide(Action<SysMenu> clickMenu)
        {
            InitializeComponent();
            GetMenus();
            ClickMenu = clickMenu;
        }
        public List<SysMenu> Menus { get; set; }
        private void GetMenus()
        {
            string path = System.Windows.Forms.Application.StartupPath + "database\\database.db;";
            ISqlConnect sqlConnect = new SqlConnect_Sl("Data Source=" + path + "");
            List<SysMenu> menuData = sqlConnect.GetObjectData<SysMenu>("select * from sys_menu");
            InitMenu initMenu = new InitMenu("0", menuData);
            Menus = initMenu.builTree();
            this.menuTree.ItemsSource = Menus;
        }

        private void menuTree_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            SysMenu menu = (SysMenu)this.menuTree.SelectedItem;
            if (menu.IsLeaf == "1")
            {
                ClickMenu.Invoke(menu);
            }

        }
    }
}
