﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace software.winwpfs.develop
{
    /// <summary>
    /// ReplaceTemplate.xaml 的交互逻辑
    /// </summary>
    public partial class ReplaceTemplate : Page
    {
        public ReplaceTemplate()
        {
            InitializeComponent();
        }
        private void CreateText(object sender, RoutedEventArgs e)
        {
            if (TemplateKey.Text.Trim() == "")
            {
                HandyControl.Controls.MessageBox.Error("模板字符不可为空！", "操作提示");
                return;
            }
            if (TemplateSql.Text.Trim() == "")
            {
                HandyControl.Controls.MessageBox.Error("模板语句不可为空！", "操作提示");
                return;
            }
            if (TemplateData.Text.Trim() == "")
            {
                HandyControl.Controls.MessageBox.Error("模板数据不可为空！", "操作提示");
                return;
            }
            string[] templateKeys = TemplateKey.Text.Trim().Split('@');
            string templateSql = TemplateSql.Text.Trim();
            int count = TemplateData.LineCount;
            string[] templateData = new string[count];
            for (int i = 0; i < count; i++)
            {
                string text = TemplateData.GetLineText(i).Replace("\r\n", "");
                templateData[i] = text;
            }

            List<string> sqls = new List<string>();
            for (int i = 0; i < templateData.Length; i++)
            {
                string[] data = templateData[i].Split('@');
                string sql = templateSql;
                for (int j = 0; j < templateKeys.Length; j++)
                {
                    sql = sql.Replace(templateKeys[j], data[j]);
                }
                sqls.Add(sql);
            }
            string lujing = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "插入语句.txt");
            File.WriteAllLines(lujing, sqls);
            MessageBoxResult messageBoxResult = HandyControl.Controls.MessageBox.Show("已生成，需要打开吗？", "打开文件", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = lujing,
                    UseShellExecute = true
                };
                Process.Start(startInfo);
            }
        }
    }
}
