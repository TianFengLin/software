﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace software.winwpfs.develop
{
    /// <summary>
    /// ContrastTextData.xaml 的交互逻辑
    /// </summary>
    public partial class ContrastTextData : Page
    {
        public ContrastTextData()
        {
            InitializeComponent();
        }
        private void ButRepeatData(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            List<string> listOne = new List<string>();
            int oneCount = DataOne.LineCount;
            for (int i = 0; i < oneCount; i++)
            {
                string lineText = DataOne.GetLineText(i).Trim();
                listOne.Add(lineText);
            }
            int twoCount = DataTwo.LineCount;
            for (int i = 0; i < twoCount; i++)
            {
                string lineText = DataTwo.GetLineText(i).Trim();
                if (listOne.Contains(lineText))
                {
                    finishtData.Add(lineText);
                }
            }
            FinishData.Text = string.Join("\r\n", finishtData);
        }
        private void ButRemoveRepeatData(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            List<string> listOne = new List<string>();
            int oneCount = DataOne.LineCount;
            for (int i = 0; i < oneCount; i++)
            {
                string lineText = DataOne.GetLineText(i).Trim();
                listOne.Add(lineText);
            }
            int twoCount = DataTwo.LineCount;
            for (int i = 0; i < twoCount; i++)
            {
                string lineText = DataTwo.GetLineText(i).Trim();
                if (!listOne.Contains(lineText))
                {
                    finishtData.Add(lineText);
                }
            }
            FinishData.Text = string.Join("\r\n", finishtData);
        }
    }
}
