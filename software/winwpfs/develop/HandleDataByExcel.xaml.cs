﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tian.Common.Texts;
using Tian.Common.Verify;
using Tian.OperationFile.excel;

namespace software.winwpfs.develop
{
    /// <summary>
    /// HandleDataByExcel.xaml 的交互逻辑
    /// </summary>
    public partial class HandleDataByExcel : Page
    {
        public HandleDataByExcel()
        {
            InitializeComponent();
        }
        private void ButChooseFile(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog("请选择一个文件夹");
            dialog.EnsureReadOnly = true;
            dialog.Filters.Add(new CommonFileDialogFilter("excel2007", "*.xlsx"));
            dialog.Filters.Add(new CommonFileDialogFilter("excel2003", "*.xls"));
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                if (string.IsNullOrEmpty(dialog.FileName))
                {
                    MessageBox.Show("文件夹路径不能为空", "提示");
                    return;
                }
                FirstPath.Text = dialog.FileName;
                DataTable dt = EpplusUtil.ExcelToDataTable("mysheet", FirstPath.Text);
                ExcelTable.ItemsSource = dt.DefaultView;
            }
        }
        private void ButRepeatData(object sender, RoutedEventArgs e)
        {
            string columnNames = ColumnNames.Text.Trim();
            if (CharVerify.IsEmpty(columnNames))
            {
                HandyControl.Controls.MessageBox.Error("列名不能为空！", "操作提示");
                return;
            }
            DataTable table = (ExcelTable.ItemsSource as DataView).ToTable();
            string[] columns = columnNames.Split(",");
            List<string> repeatData = new List<string>();
            foreach (string columnName in columns)
            {
                List<string> list = new List<string>();
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    string value = table.Rows[i][columnName].ToString();
                    if (!list.Contains(value))
                    {
                        list.Add(value);
                    }
                    else
                    {
                        repeatData.Add(columnName + ":" + value);
                    }
                }
            }
            LastData.Text = string.Join("\r\n", repeatData);
        }
        private void ButDeleteBlank(object sender, RoutedEventArgs e)
        {

        }

        private void ButSaveData(object sender, RoutedEventArgs e)
        {
            string columnNames = ColumnNames.Text.Trim();
            if (CharVerify.IsEmpty(columnNames))
            {
                HandyControl.Controls.MessageBox.Error("列名不能为空！", "操作提示");
                return;
            }
            string columnValues = ColumnValues.Text.Trim();
            if (CharVerify.IsEmpty(columnValues))
            {
                HandyControl.Controls.MessageBox.Error("列值不能为空！", "操作提示");
                return;
            }
            List<string> values = columnValues.Split(",").ToList();
            DataTable table = (ExcelTable.ItemsSource as DataView).ToTable();
            DataTable source = table.Clone();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string value = table.Rows[i][columnNames].ToString();
                if (values.Contains(value))
                {
                    source.Rows.Add(table.Rows[i].ItemArray);
                }
            }
            EpplusUtil.DataTableToExcel(source, "mysheet", @"C:\Users\20211105\Desktop\淮安人大\1.xlsx");
            HandyControl.Controls.MessageBox.Success("导出成功！", "操作提示");
        }
        private void ButExcludeData(object sender, RoutedEventArgs e)
        {
            string columnNames = ColumnNames.Text.Trim();
            if (CharVerify.IsEmpty(columnNames))
            {
                HandyControl.Controls.MessageBox.Error("列名不能为空！", "操作提示");
                return;
            }
            string columnValues = ColumnValues.Text.Trim();
            if (CharVerify.IsEmpty(columnValues))
            {
                HandyControl.Controls.MessageBox.Error("列值不能为空！", "操作提示");
                return;
            }
            List<string> values = columnValues.Split(",").ToList();
            DataTable table = (ExcelTable.ItemsSource as DataView).ToTable();
            DataTable source = table.Clone();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string value = table.Rows[i][columnNames].ToString();
                if (!values.Contains(value))
                {
                    source.Rows.Add(table.Rows[i].ItemArray);
                }
            }
            EpplusUtil.DataTableToExcel(source, "mysheet", @"C:\Users\20211105\Desktop\淮安人大\2.xlsx");
            HandyControl.Controls.MessageBox.Success("导出成功！", "操作提示");
        }
        private void ButSplitCount(object sender, RoutedEventArgs e)
        {
            string columnNames = ColumnNames.Text.Trim();
            if (CharVerify.IsEmpty(columnNames))
            {
                HandyControl.Controls.MessageBox.Error("列名不能为空！", "操作提示");
                return;
            }
            DataTable table = (ExcelTable.ItemsSource as DataView).ToTable();
            table.Columns.Add("count");
            DataTable source = table.Clone();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                string value = table.Rows[i][columnNames].ToString();
                if (StringUtil.GetLastChar(value) == ",")
                {
                    value = StringUtil.RemoveLastChar(value);
                }
                string[] values = value.Split(",");
                table.Rows[i]["count"] = values.Length;
                source.Rows.Add(table.Rows[i].ItemArray);
            }
            EpplusUtil.DataTableToExcel(source, "mysheet", @"C:\Users\20211105\Desktop\淮安人大\3.xlsx");
            HandyControl.Controls.MessageBox.Success("导出成功！", "操作提示");
        }
    }
}
