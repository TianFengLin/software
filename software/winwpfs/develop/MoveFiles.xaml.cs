﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Tian.Common.Base;

namespace software.winwpfs.develop
{
    /// <summary>
    /// MoveFiles.xaml 的交互逻辑
    /// </summary>
    public partial class MoveFiles : Page
    {
        public MoveFiles()
        {
            InitializeComponent();
        }

        private void ReadyCopy(object sender, RoutedEventArgs e)
        {
            //初始文件
            string firstFileRoot = FirstFileRoot.Text.Trim();
            List<string> firstFiles = new List<string>();
            int firstCount = FirstData.LineCount;
            for (int i = 0; i < firstCount; i++)
            {
                string lineText = FirstData.GetLineText(i);
                if (firstFileRoot == "")
                {
                    firstFiles.Add(lineText.TrimEnd().TrimStart());
                }
                else
                {
                    firstFiles.Add(firstFileRoot + System.IO.Path.DirectorySeparatorChar + lineText.TrimEnd().TrimStart());
                }

            }
            string finishFileRoot = FinishFileRoot.Text.Trim();
            // 目标路径
            List<string> finishFiles = new List<string>();
            int finishCount = FinishData.LineCount;
            for (int i = 0; i < finishCount; i++)
            {
                string lineText = FinishData.GetLineText(i);
                if (finishFileRoot == "")
                {
                    finishFiles.Add(lineText.TrimEnd().TrimStart());
                }
                else
                {
                    finishFiles.Add(finishFileRoot + System.IO.Path.DirectorySeparatorChar + lineText.TrimEnd().TrimStart());
                }

            }
            if (firstCount != finishCount)
            {
                HandyControl.Controls.MessageBox.Error("初始文件与最终文件数量不一致！", "操作提示");
                return;
            }
            List<string> notFiles = new List<string>();
            // 开始复制文件
            FileUtil fileHelper = new FileUtil();
            for (int i = 0; i < firstCount; i++)
            {
                if (!File.Exists(firstFiles[i]))
                {
                    notFiles.Add(firstFiles[i]);
                    continue;
                }
                FileInfo file = new FileInfo(firstFiles[i]);
                string finishFile = finishFiles[i];
                string[] filePaths = finishFile.Split(@"\");
                string filePathLast = JoinPath(filePaths, filePaths.Length - 1);
                //判断文件夹会否存在
                if (!Directory.Exists(filePathLast))
                {
                    Directory.CreateDirectory(filePathLast);
                }
                if (!File.Exists(finishFiles[i]))
                {
                    FileStream fileStream = File.Create(finishFile);
                    fileStream.Close();
                }
                file.CopyTo(finishFiles[i], true);
            }
            string lujing = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "插入语句.txt");
            File.WriteAllLines(lujing, notFiles);
            MessageBoxResult messageBoxResult = HandyControl.Controls.MessageBox.Show("复制完成，需要打开文件夹吗？", "打开文件", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = finishFileRoot,
                    UseShellExecute = true
                };
                Process.Start(startInfo);
            }

        }

        public string JoinPath(string[] path, int length)
        {
            string result = "";
            for (int i = 0; i < length; i++)
            {
                if (result == "")
                {
                    result = path[i];
                }
                else
                {
                    result = result + System.IO.Path.DirectorySeparatorChar + path[i];
                }

            }
            return result;

        }
    }
}
