﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace software.winwpfs.develop
{
    /// <summary>
    /// TextFormat.xaml 的交互逻辑
    /// </summary>
    public partial class TextFormat : Page
    {
        public TextFormat()
        {
            InitializeComponent();
        }
        private void StringJoint(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            int lineCount = FirstData.LineCount;
            string finishText = "";
            string key = TemplateKey.Text;
            for (int i = 0; i < lineCount; i++)
            {
                string lineText = FirstData.GetLineText(i);
                finishText += lineText.Trim() + key;
            }
            FinishData.Text = finishText;
        }
        private void ToLower(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            int lineCount = FirstData.LineCount;
            for (int i = 0; i < lineCount; i++)
            {
                string lineText = FirstData.GetLineText(i);
                finishtData.Add(lineText.Trim().ToLower());
            }
            FinishData.Text = string.Join("\r\n", finishtData);
        }
        private void ToUpper(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            int lineCount = FirstData.LineCount;
            for (int i = 0; i < lineCount; i++)
            {
                string lineText = FirstData.GetLineText(i);
                finishtData.Add(lineText.Trim().ToUpper());
            }
            FinishData.Text = string.Join("\r\n", finishtData);
        }
        private void ButRepeatData(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            List<string> list = new List<string>();
            int lineCount = FirstData.LineCount;
            for (int i = 0; i < lineCount; i++)
            {
                string lineText = FirstData.GetLineText(i).Trim();
                if (list.Contains(lineText))
                {
                    finishtData.Add(lineText);
                }
                else
                {
                    list.Add(lineText);
                }
            }
            FinishData.Text = string.Join("\r\n", finishtData);
        }
        private void ButDataCount(object sender, RoutedEventArgs e)
        {
            List<string> finishtData = new List<string>();
            int lineCount = FirstData.LineCount;
            finishtData.Add(lineCount.ToString());
            FinishData.Text = string.Join("\r\n", finishtData);
        }

    }
}
