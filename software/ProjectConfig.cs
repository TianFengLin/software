﻿using Newtonsoft.Json.Linq;
using software.config;
using software.config.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace software
{
    public static class ProjectConfig
    {
        /// <summary>
        /// 获取数据库配置
        /// </summary>
        /// <returns></returns>
        public static void GetDataBaseSetting()
        {
            string path = System.Windows.Forms.Application.StartupPath + "config" + Path.DirectorySeparatorChar + "json" + Path.DirectorySeparatorChar + "database.json";
            string json = File.ReadAllText(path, Encoding.UTF8);
            JObject jo = JObject.Parse(json);
            string dataBases = jo["dataBase"].ToString();
            JArray array = JArray.Parse(dataBases);
            for (int i = 0; i < array.Count; i++)
            {
                DataBase dataBase = new DataBase();
                dataBase.Name = array[i]["name"].ToString();
                dataBase.Key = array[i]["key"].ToString();
                dataBase.Ip = array[i]["ip"].ToString();
                dataBase.Port = array[i]["port"].ToString();
                dataBase.BaseName = array[i]["baseName"].ToString();
                dataBase.BaseType = array[i]["baseType"].ToString();
                dataBase.UserName = array[i]["userName"].ToString();
                dataBase.Password = array[i]["password"].ToString();
                AppSetting.dataBase.Add(dataBase);
            }
        }
    }
}
