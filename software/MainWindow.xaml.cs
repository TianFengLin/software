﻿using System;
using System.Diagnostics;
using MaterialDesignThemes.Wpf;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using software.connect;
using software.model;
using System.Collections.Generic;
using software.winwpfs.layout;

namespace software
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SetMenuAndTabLayout();
            this.SizeChanged += new SizeChangedEventHandler(FormChangeSize);
        }
        /// <summary>
        /// 自适应大小
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void FormChangeSize(object sender, SizeChangedEventArgs args)
        {
            //可见高度-头尾高度
            double height = this.ActualHeight - 140;
            GridLength gridLength = new GridLength(height);
            tabContentRow.Height = gridLength;

        }
        /// <summary>
        /// 设置菜单与选项卡
        /// </summary>
        private void SetMenuAndTabLayout()
        {
            TabContent tabContent = new TabContent();
            LeftSide leftSide = new LeftSide(tabContent.AddTab);
            this.GridMenu.Children.Add(leftSide);
            this.GridTab.Children.Add(tabContent);
        }
        //窗体加载后触发
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            //获取数据库配置
            ProjectConfig.GetDataBaseSetting();
        }
    }
}
