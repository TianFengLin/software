﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace software.model
{
    public class SysMenu
    {
        private string menuId;
        private string menuName;
        private string parentId;
        private string url;
        private string icon;
        private string isLeaf;
        private int listId;
        private List<SysMenu> children;
        /// <summary>
        /// 菜单编号
        /// </summary>
        public string MenuId { get => menuId; set => menuId = value; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName { get => menuName; set => menuName = value; }
        /// <summary>
        /// 父节点
        /// </summary>
        public string ParentId { get => parentId; set => parentId = value; }
        /// <summary>
        /// 窗口地址
        /// </summary>
        public string Url { get => url; set => url = value; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get => icon; set => icon = value; }
        /// <summary>
        /// 是否是子节点
        /// </summary>
        public string IsLeaf { get => isLeaf; set => isLeaf = value; }
        /// <summary>
        /// 排序
        /// </summary>
        public int ListId { get => listId; set => listId = value; }
        /// <summary>
        /// 子菜单
        /// </summary>
        public List<SysMenu> Children { get => children; set => children = value; }
    }
}
