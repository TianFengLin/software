﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace software.config.model
{
    public class DataBase
    {
        private string name;

        private string key;

        private string ip;

        private string port;

        private string baseName;

        private string baseType;

        private string userName;

        private string password;
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get => name; set => name = value; }
        /// <summary>
        /// 键
        /// </summary>
        public string Key { get => key; set => key = value; }
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string BaseName { get => baseName; set => baseName = value; }
        /// <summary>
        /// 数据库类型
        /// </summary>
        public string BaseType { get => baseType; set => baseType = value; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get => userName; set => userName = value; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get => password; set => password = value; }
        /// <summary>
        /// IP
        /// </summary>
        public string Ip { get => ip; set => ip = value; }
        /// <summary>
        /// 端口
        /// </summary>
        public string Port { get => port; set => port = value; }
    }
}
