﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace software.business.develop
{
    public class DataBaseVM : DependencyObject
    {
        public static readonly DependencyProperty NameProperty = DependencyProperty.Register("Name", typeof(string), typeof(DataBaseVM));
        public string Name
        {
            set { SetValue(NameProperty, value); }
            get { return (string)GetValue(NameProperty); }
        }



        public static readonly DependencyProperty AgeProperty = DependencyProperty.Register("Age", typeof(string), typeof(DataBaseVM));
        public string Key
        {
            set { SetValue(AgeProperty, value); }
            get { return (string)GetValue(AgeProperty); }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
