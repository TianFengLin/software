﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace software.business.develop
{
    public class ReplaceTemplateVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private string templateKey;

        public string TemplateKey
        {
            get
            {
                return templateKey;
            }
            set
            {
                templateKey = value;
                PropertyChanged(this, new PropertyChangedEventArgs("templateKey"));
            }
        }
    }
}
